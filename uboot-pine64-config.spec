# Based on Manjaro package 'uboot-soquartz-cm4'
# More at https://gitlab.manjaro.org/manjaro-arm/packages/core/uboot-soquartz-cm4
%global devicetree rk3566-soquartz-cm4.dtb

%define debug_package %{nil}

Name:           uboot-pine64-config
Version:        2022.04.rc1
Release:        2%{?dist}
Summary:        Package that configures u-boot on a Quartz64 board
License:        GPL-2.0+

ExclusiveArch:  aarch64

URL:            http://www.denx.de/wiki/U-Boot/WebHome

Source:         https://gitlab.com/pgwipeout/u-boot-quartz64/-/archive/quartz64/u-boot-quartz64-quartz64.tar.gz
Source1:        https://github.com/JeffyCN/rockchip_mirrors/raw/6186debcac95553f6b311cee10669e12c9c9963d/bin/rk35/rk3568_bl31_v1.28.elf
Source2:        https://github.com/JeffyCN/rockchip_mirrors/raw/ddf03c1d80b33dac72a33c4f732fc5849b47ff99/bin/rk35/rk3568_ddr_1560MHz_v1.13.bin

BuildRequires:  dtc
BuildRequires:  bc
BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  python3
BuildRequires:  openssl-devel

BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros

Requires:       grubby
Requires:       gzip

Provides:       uboot

%description
Simple package that installs the right files in the right places to allow a Quartz64 board to boot with u-boot

%prep
%autosetup -n u-boot-quartz64-quartz64

cp %{SOURCE1} bl31.elf
cp %{SOURCE2} ram_init.bin

%build

# Avoid build warnings by editing a .config option in place instead of
# appending an option to .config, if an option is already present
update_config() {
  if ! grep -q "^$1=$2$" .config; then
    if grep -q "^# $1 is not set$" .config; then
      sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
    elif grep -q "^$1=" .config; then
      sed -i -e "s/^$1=.*/$1=$2/g" .config
    else
      echo "$1=$2" >> .config
    fi
  fi
}

%{__make} quartz64-b-rk3566_defconfig
update_config 'CONFIG_IDENT_STRING' '" Red Hat Enterprise Linux for ARM64"'
update_config 'CONFIG_OF_CONTROL' 'y' >> .config

%{__make} EXTRAVERSION=-%{release}


%install
mkdir -p %{buildroot}/%{efi_esp_root}/extlinux

install -D -m 0644 idbloader.img u-boot.itb -t %{buildroot}%{efi_esp_root}


%post
ROOTDEV=$(grubby --info=$(grubby --default-kernel) | grep root= | cut -d"\"" -f 2)

echo "LABEL \"Red Hat Enterprise Linux ARM64\"
KERNEL /Image
FDT /dt.dtb
APPEND initrd=/initramfs earlycon=uart8250,mmio32,0xfe660000 root=$ROOTDEV ro" > %{efi_esp_root}/extlinux/extlinux.conf

# Copy kernel, initramfs and device tree
ver=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 2)
rel=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 3)
vmlinuz=/boot/vmlinuz-$ver-$rel
initramfs=/boot/initramfs-$ver-$rel.img
dtb=/boot/dtb-$ver-$rel/rockchip/%{devicetree}
install $vmlinuz %{efi_esp_root}/Image.gz
install $initramfs %{efi_esp_root}/initramfs
install $dtb %{efi_esp_root}/dt.dtb

gzip -df %{efi_esp_root}/Image.gz

%files
%license Licenses/*
%doc README MAINTAINERS
%{efi_esp_root}/extlinux
%{efi_esp_root}/idbloader.img
%{efi_esp_root}/u-boot.itb

%changelog
* Thu Feb 09 2023 Luca Magrone <luca@magrone.cc> - 2022.04.rc1-2
- Change source 1 and 2

* Tue Feb 07 2023 Luca Magrone <luca@magrone.cc> - 2022.04.rc1-1
- Tell gzip to forcibly unzip Image
- Update source links
- Change APPEND entry
- Match version with source

* Sun Jan 29 2023 Luca Magrone <luca@magrone.cc> - 2022.01.rc4-5
- Fix extlinux config file path

* Sun Jan 29 2023 Luca Magrone <luca@magrone.cc> - 2022.01.rc4-4
- Enable CONFIG_OF_CONTROL
- specfile: fix typo in devicetree global

* Sun Jan 29 2023 Luca Magrone <luca@magrone.cc> - 2022.01.rc4-3
- Install files in the efi partition

* Sat Jan 21 2023 Luca Magrone <luca@magrone.cc> - 2022.01.rc4-2
- Fix extlinux configuration path in post script

* Sat Jan 21 2023 Luca Magrone <luca@magrone.cc> - 2022.01.rc4-1
- Initial package release
